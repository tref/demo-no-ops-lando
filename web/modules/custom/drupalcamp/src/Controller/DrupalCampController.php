<?php

namespace Drupal\drupalcamp\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for DrupalCamp module routes.
 */
class DrupalCampController extends ControllerBase {

  /**
   * Demo listener.
   *
   * @return array
   *   A render-able array for a demo page.
   */
  public function demo() {
    return [
      '#markup' => $this->t('Hello DrupalCamp!'),
    ];
  }

}
