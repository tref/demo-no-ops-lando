<?php

/**
 * @file
 * Local settings.
 */

$databases['default']['default'] = [
  'database' => 'drupal8',
  'username' => 'drupal8',
  'password' => 'drupal8',
  'prefix' => '',
  'host' => 'database',
  'port' => '',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
];

$settings['hash_salt'] = 'aSLvvU91H6PWEPAjbYW52ZjPHYFkDHEFUH6tQORjGfysUDGos_OUqjvDgzBMrF51GEck7X1icQ';
$config_directories[CONFIG_SYNC_DIRECTORY] = '/app/config/sync';

